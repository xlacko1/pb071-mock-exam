/*
 * Here you can write any of your unit tests. We strongly encourage you to remove this file and create
 * only your own files in format `tests-<<SOME_GROUP>>.c` so even the name of the file explains
 * what is/should be tested there.
 *
 * Please be aware that any translation unit (.c file) you whish to test must be specified in the
 * UNIT_TESTED_FILES variable in CMakeLists.txt, otherwise it won't be compiled with the tests
 * and the symbol won't be reachable. (e.g. linker will complain and the compilation will fail)
 */

#define CUT
#include "libs/cut.h"

#include <stdlib.h>

/*
TEST(BASIC_TESTCASE)
{
    CHECK(0 == 0);
}
*/
