#include <stdlib.h>
#include <stdio.h>

/**
 * This file should only contain the main function and
 * command line arguments processing logic. You shouldn't use it
 * for any logic apart from that. Create some own .h/.c files by yourselves.
 */
int main(void)
{
	printf("find5\n");
	return EXIT_SUCCESS;
}
