#!/bin/false

example_output() {
	# This test has an example output file, example_output.out
	echo "OK"
}

run 'Simple output' -- example_output

example_exit() {
	exit 1
}

run -efail 'Should exit with 1' -- example_exit
