#!/bin/false

suite_setup() {
    create_path empty_subdir
    create_path subdir
    create_file 1 1
    create_file a 2
    create_file .hidden 10
    create_file .hidden2 20
    create_file empty_file 0
    create_file subdir/file.txt 6
}

nanecisto_noargs() {
    $exe
}

run 'Spuštění bez argumentů' -- nanecisto_noargs

nanecisto_size() {
    $exe -s s
}

run "Řazení dle velikosti" -- nanecisto_size

nanecisto_empty() {
    $exe empty_subdir
}

run "Prázdný podadresář" -- nanecisto_empty

nanecisto_subdir() {
    $exe subdir
}

run "Podadresář s jediným souborem" -- nanecisto_subdir

nanecisto_hidden() {
    $exe -a
}

run "Skrytý soubor" -- nanecisto_hidden

nanecisto_file_and_dir() {
    $exe subdir -n file.txt
}

run "Podadresář a název souboru" -- nanecisto_file_and_dir

nanecisto_more_args() {
    $exe -n .hidden -a .
}

run "Více argumentů následovaných adresářem" -- nanecisto_more_args
