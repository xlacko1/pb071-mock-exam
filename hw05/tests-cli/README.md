# CLI tests for find5

By now, you should be pretty familiar with the logic of the CLI test-suite, so don't expect many
details here.

Given the freedom you have in the way how this task could be implemented, the vast majority of
your testing should be focused here, because we're always testing your code as a whole so the only
interface our Kontr knows of is the command line interface.

## General structure

You can write your tests however you like, but we'd suggest the following structure:

* As a first step create some directory structure you'd like to test and create file `tests-<<SOME_CASE>>.sh`.
  * Describe such structure in `suite_setup` function using the supplementary functions described below.
* Then prepare some test cases against this structure and create test functions for those.
  * e.g. specific command line options, subdirectories etc.
* Prepare the files containing expected standard output and standard error output of your program and store them in `data` directory.
* Rinse and repeat until you have all the test cases you need.


## Supportive functions

For your convenience, we have added two shell functions which you can use in the `suite_setup`.

`create_path`

* Function expects relative path that will be created in the temporary directory used by tests execution.
* It can describe some deeper structure e.g. `firstLevel/secondLevel/thirdLevel` which will create three directories.

`create_file`

* Function creates a file on the specified place relative to the testing directory.
* It takes two arguments:
  * Path to the new file
  * Size of the created file [optional]
  * If the size is not set, the file will be empty.
* If the path to the created file doesn't exist, it will be created by the `create_path` function.

