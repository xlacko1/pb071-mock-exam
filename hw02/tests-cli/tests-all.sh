#!/bin/false

run -0 valid.in -1 valid.out -2 valid.err "Validation (--check-valid)" -- \
	$exe --load --check-valid

run -0 elimination.in -1 elimination.out -2 elimination.err "Eliminations (--eliminate-*)" -- \
	$exe --load --eliminate-row 3 --eliminate-col 0 --eliminate-box 03 --print

run -0 load.in -1 load.out -2 load.err "Loading (--load, --print)" -- \
	$exe --load --raw --load --print

run -0 needs-solving.in -1 needs-solving.out -2 needs-solving.err "Unsolved sudoku detection (--needs-solving)" -- \
	$exe --load --needs-solving

run -0 simple-full-solve.in -1 simple-full-solve.out -2 simple-full-solve.err "Solving (--solve, --print)" -- \
	$exe --load --solve --print
