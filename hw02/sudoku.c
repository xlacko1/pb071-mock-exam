#include "sudoku.h"

// TODO: Remove this macro and all its uses in the code before submitting
//       your solution for review!
#define UNUSED(A) ((void) (A))

/* ************************************************************** *
 *               Functions required by assignment                 *
 * ************************************************************** */

bool eliminate_row(unsigned int sudoku[9][9], int row_index)
{
    UNUSED(sudoku);
    UNUSED(row_index);
    return false; // todo
}

bool eliminate_col(unsigned int sudoku[9][9], int col_index)
{
    UNUSED(sudoku);
    UNUSED(col_index);
    return false; // todo
}

bool eliminate_box(unsigned int sudoku[9][9], int row_index, int col_index)
{
    UNUSED(sudoku);
    UNUSED(row_index);
    UNUSED(col_index);
    return false; // todo
}

bool needs_solving(unsigned int sudoku[9][9])
{
    UNUSED(sudoku);
    return true; // todo
}

bool is_valid(unsigned int sudoku[9][9])
{
    UNUSED(sudoku);
    return false; // todo
}

bool solve(unsigned int sudoku[9][9])
{
    UNUSED(sudoku);
    return false; // todo
}

bool load(unsigned int sudoku[9][9])
{
    UNUSED(sudoku);
    return false; // todo
}

void print(unsigned int sudoku[9][9])
{
    UNUSED(sudoku);
    return; // todo
}

/* ************************************************************** *
 *                              Bonus                             *
 * ************************************************************** */

#ifdef BONUS_GENERATE
void generate(unsigned int sudoku[9][9])
{
    return; // todo
}
#endif

#ifdef BONUS_GENERIC_SOLVE
bool generic_solve(unsigned int sudoku[9][9])
{
    return false; // todo
}
#endif

/* ************************************************************** *
 *                 Adwised auxiliary functionns                   *
 * ************************************************************** */

/* TODO: comment-out #if 0 and correspoding endif to implement */

#if 0
/**
 * @brief Compute the bitset of all done numbers in the box.
 *
 * You might like a similar function for row and for column.
 *
 * @param sudoku 2D array of digit bitsets
 * @param row_index of the top most row in box, one of 0, 3, 6
 * @param col_index of the left most column in box, one of 0, 3, 6
 */
static int box_bitset(unsigned int sudoku[9][9], int row_index, int col_index) {
    return 0;
}

/**
 * @brief Add number into bit set
 *
 * This function encapsulates a bit ands, ors and whatever
 * other bint operations, that would flood the toplevel code
 * with implementation details.
 *
 * @param original  contents of the 2D sudoku cell.
 * @param number    to be added to the set
 *
 * @return          new value of the cell with the number included
 */
static unsigned int bitset_add(unsigned int original, int number) {
    return 0;
}

/**
 * @brief  Drop number from bit set.
 *
 * For detailed description, see bitset_add.
 *
 * @param original  contents of the 2D sudoku cell.
 * @param number    to be dropped from the set
 *
 * @return          new value of the cell without the number included
 */
static unsigned int bitset_drop(unsigned int original, int number) {
    return 0;
}

/**
 * @brief  Check whether given number is present in the set.
 *
 * @param original  contents of the 2D sudoku cell.
 * @param query     number which should be checked for presence
 *
 * @return          true if set, false otherwise
 */
static bool bitset_is_set(unsigned int original, int query) {
	return false;
}

/**
 * @brief  Check whether given cell has a unique value assigned.
 *
 * @param original  bitset to check for single vs. multiple values.
 *
 * @return          true if set, false otherwise
 */
static bool bitset_is_unique(unsigned int original) {
	return false;
}

/**
 * @brief Return next number present in bit set.
 *
 * This function encapsulates a bit ands, ors and whatever
 * other bint operations, that would flood the toplevel code
 * with implementation details.
 *
 * @param original  contents of the 2D sudoku cell.
 * @param previous  last known number present, 0 for start
 *
 * @return          * next (higher) number than argument if such
                    such is present.
 *                  * -1 otherwise
 *
 * @note The value previous might not be in the bitset
 */
static int bitset_next(unsigned int bitset, int previous) {
    return 0;
}

#endif // if 0
