#ifndef UTILS_H
#define UTILS_H

#include "mainwrap.h"

# define CHECK_STD_FILE(f, content) do {                                            \
    if (!cut_File(f, content)) {                                                \
        cut_Check("content of '" #f "' is not equal", __FILE__, __LINE__);          \
    } } while(0)

# define CHECK_BINARY_STD_FILE(f, bytes) do {                                            \
    if (!cut_BinaryFile(f, sizeof(bytes), bytes)) {                                                \
        cut_Check("content of '" #f "' is not equal", __FILE__, __LINE__);          \
    } } while(0)

#define TEST_ENCODE_MAIN(STATUS) \
    do {                 \
        CHECK(app_main("-e") == (STATUS));                             \
    } while (0)
#define TEST_ENCODE(INPUT, OUTPUT)                         \
    do {                                                      \
        INPUT_BYTES(INPUT);                                \
        TEST_ENCODE_MAIN(EXIT_SUCCESS);                                                   \
        CHECK_STD_FILE(stderr, "");                                                   \
        CHECK_BINARY_STD_FILE(stdout, OUTPUT);                       \
    } while (0)

#define TEST_DECODE_MAIN(STATUS) \
    do {                 \
        CHECK(app_main("-d") == (STATUS));                             \
    } while (0)

#define TEST_DECODE_FAIL_NO_OUTPUT(INPUT)                         \
    do {                                                      \
        INPUT_BYTES(INPUT);                                          \
        TEST_DECODE_MAIN(EXIT_FAILURE);                              \
    } while (0)

#define TEST_DECODE_NO_STDERR(INPUT, OUTPUT)                         \
    do {                                                   \
        INPUT_BYTES(INPUT);                                          \
        TEST_DECODE_MAIN(EXIT_SUCCESS);                              \
        CHECK_BINARY_STD_FILE(stdout, OUTPUT);                       \
    } while (0)

#define TEST_DECODE(INPUT, OUTPUT)                         \
    do {                                                   \
        INPUT_BYTES(INPUT);                                          \
        TEST_DECODE_MAIN(EXIT_SUCCESS);                              \
        CHECK_STD_FILE(stderr, "");                       \
        CHECK_BINARY_STD_FILE(stdout, OUTPUT);                       \
    } while (0)

#endif // UTILS_H
