/*
 * You can write your own tests in this file
 */

#define CUT
#include "libs/cut.h"
#include "libs/mainwrap.h"

#include <stdlib.h>

#include "../sudoku.h"

/* This is an example test that is not a part of sanity tests in Kontr. 
 * But still, you should be able to pass it.
 */
TEST(trivial_needs_solving) {
    // This creates an prefiled sudoku.
    unsigned int sudoku[9][9] = {
        // Sudoku encoded using hex:
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
        { 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04, 0x02, 0x04 },
    };

    ASSERT(!needs_solving(sudoku));
}

// Test template just for you ;)
/*
TEST(TODO_NAME_MUST_BE_UNIQUE)
{
    const unsigned char input[] = {
        // TODO
    };
    const unsigned char output[] = {
        // TODO
    };

    INPUT_BYTES(input);
    CHECK(app_main(TODO) == EXIT_SUCCESS);
    CHECK_FILE(stderr, TODO);
    CHECK_BINARY_FILE(stdout, output);
}
*/
