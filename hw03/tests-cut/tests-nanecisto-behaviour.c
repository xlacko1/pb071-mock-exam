#include "libs/cut.h"

#include <stdio.h>
#include <stdlib.h>

#include "libs/utils.h"
#include "../cpu.h"

/* Macros for file paths */
#define RUN_FILE             "../tests-cut/data/nanecisto-run.bin" // hw03/build/../tests-cut/data/binary.bin
#define INVALID_INSTR_FILE   "../tests-cut/data/nanecisto-invalid-instr.bin"
#define INVALID_REG_FILE     "../tests-cut/data/nanecisto-invalid-reg.bin"
#define INVALID_ADDRESS_FILE "../tests-cut/data/nanecisto-invalid-address.bin"

/* Tests from assignment */
/* #desc: Run on nop */
TEST(nanecisto_run_nop)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, RUN_FILE) == 0);

    // Run
    int steps = 5;
    int retval = cpu_run(cpu, steps);

    CHECK(retval == steps);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Run on halt */
TEST(nanecisto_run_halt)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, RUN_FILE) == 0);

    // Run
    int steps = 15;
    int retval = cpu_run(cpu, steps);

    CHECK(retval == 10);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Invalid instruction */
TEST(nanecisto_invalid_instr)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, INVALID_INSTR_FILE) == 0);

    // Run
    int steps = 2;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == -1);
    CHECK(cpu_get_status(cpu) == CPU_ILLEGAL_INSTRUCTION);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Invalid reg */
TEST(nanecisto_invalid_reg)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, INVALID_REG_FILE) == 0);

    // Run
    int steps = 2;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == -1);
    CHECK(cpu_get_status(cpu) == CPU_ILLEGAL_OPERAND);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Run on invalid address */
TEST(nanecisto_cpu_run_invalid_address)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, INVALID_ADDRESS_FILE) == 0);

    // Run
    int steps = 3;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == -3);
    CHECK(cpu_get_status(cpu) == CPU_INVALID_ADDRESS);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}
