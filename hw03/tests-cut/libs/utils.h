#ifndef UTILS_H
#define UTILS_H

#include <stdbool.h>
#include <stdio.h>
#include "../../cpu.h"

int prepare_cpu(struct cpu **cpu, FILE **fptr, const char* file_name);

#endif /* UTILS_H */