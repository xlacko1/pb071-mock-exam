#include "utils.h"

int prepare_cpu(struct cpu **cpu, FILE **fptr, const char* file_name)
{
    *fptr = fopen(file_name, "rb");
    if (*fptr == NULL) {
        return 1;
    }

    size_t stack_capacity = 256; // Set some stack capacity
    int32_t *stack_ptr = NULL;
    int32_t *memory = cpu_create_memory(*fptr, stack_capacity, &stack_ptr);
    if (memory == NULL) {
        fclose(*fptr);
        *fptr = NULL;
        return 1;
    }

    *cpu = cpu_create(memory, stack_ptr, stack_capacity);

    return 0;
}
