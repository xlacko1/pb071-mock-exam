#include "libs/cut.h"

#include <stdio.h>
#include <stdlib.h>

#include "libs/utils.h"
#include "../cpu.h"

/* Macros for file paths */
#define NOP_FILE   "../tests-cut/data/nanecisto-nop.bin" // hw03/build/../tests-cut/data/binary.bin
#define HALT_FILE  "../tests-cut/data/nanecisto-halt.bin"
#define ADD_FILE   "../tests-cut/data/nanecisto-add.bin"
#define SUB_FILE   "../tests-cut/data/nanecisto-sub.bin"
#define INC_FILE   "../tests-cut/data/nanecisto-inc.bin"
#define DEC_FILE   "../tests-cut/data/nanecisto-dec.bin"
#define MOVR_FILE  "../tests-cut/data/nanecisto-movr.bin"
#define SWAP_FILE  "../tests-cut/data/nanecisto-swap.bin"
#define PUSH_FILE  "../tests-cut/data/nanecisto-push.bin"
#define POP_FILE   "../tests-cut/data/nanecisto-pop.bin"
#define LOAD_FILE  "../tests-cut/data/nanecisto-load.bin"
#define STORE_FILE "../tests-cut/data/nanecisto-store.bin"

/* Tests from assignment */
/* #desc: Instruction nop */
TEST(nanecisto_nop)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, NOP_FILE) == 0);

    // Run
    int steps = 3;
    int retval = cpu_run(cpu, steps);

    CHECK(retval == steps);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction halt */
TEST(nanecisto_halt)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, HALT_FILE) == 0);

    // Run
    int steps = 2;
    int retval = cpu_run(cpu, steps);

    CHECK(retval == steps - 1);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction add */
TEST(nanecisto_add)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, ADD_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 4);

    // Run
    int steps = 1;
    int retval = cpu_run(cpu, steps);

    //Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 3);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 2);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 3);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 4);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction sub */
TEST(nanecisto_sub)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, SUB_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 4);
    cpu_set_register(cpu, REGISTER_B, 3);
    cpu_set_register(cpu, REGISTER_C, 2);
    cpu_set_register(cpu, REGISTER_D, 1);

    // Run
    int steps = 1;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 1);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 3);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 2);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 1);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction inc */
TEST(nanecisto_inc)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, INC_FILE) == 0);

    // Run
    int steps = 1;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 1);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction dec */
TEST(nanecisto_dec)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, DEC_FILE) == 0);

    // Run
    int steps = 1;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == -1);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction movr */
TEST(nanecisto_movr)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, MOVR_FILE) == 0);

    // movr a x
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 1);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 256);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 65535);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 0);

    // movr b x
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 1);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 256);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 65535);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 0);

    // movr c x
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 1);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 256);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 65535);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 0);

    // movr d x
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 1);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 256);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 65535);
    CHECK(cpu_step(cpu) != 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction swap */
TEST(nanecisto_swap)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, SWAP_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 7);
    cpu_set_register(cpu, REGISTER_B, 3);
    cpu_set_register(cpu, REGISTER_C, 9);
    cpu_set_register(cpu, REGISTER_D, 5);

    // Run
    int steps = 4;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 7);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 9);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 5);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 3);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction push */
TEST(nanecisto_push)
{
    // Allocations
    FILE *fptr = fopen(PUSH_FILE, "rb");
    ASSERT(fptr != NULL);
    size_t stack_capacity = 1;
    int32_t *stack_ptr = NULL;
    int32_t *memory = cpu_create_memory(fptr, stack_capacity, &stack_ptr);
    ASSERT(memory != NULL);
    struct cpu *cpu = cpu_create(memory, stack_ptr, stack_capacity);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 0);

    // Run
    int steps = 1;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_stack_size(cpu) == 1);
    CHECK(stack_ptr[0] == 2); // push b
    CHECK(cpu_get_status(cpu) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction pop */
TEST(nanecisto_pop)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, POP_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 0);

    // Run
    int steps = 2;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 1);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 2);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 2);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction load */
TEST(nanecisto_load)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, LOAD_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 0);

    // Run
    int steps = 2;
    int retval = cpu_run(cpu, steps);

    // Check
    CHECK(retval == steps);
    CHECK(cpu_get_register(cpu, REGISTER_A) == 2);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 2);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 3);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Tests from assignment */
/* #desc: Instruction store */
TEST(nanecisto_store)
{
    // Allocations
    FILE *fptr = fopen(STORE_FILE, "rb");
    ASSERT(fptr != NULL);
    size_t stack_capacity = 1;
    int32_t *stack_ptr = NULL;
    int32_t *memory = cpu_create_memory(fptr, stack_capacity, &stack_ptr);
    ASSERT(memory != NULL);
    struct cpu *cpu = cpu_create(memory, stack_ptr, stack_capacity);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 0);

    // Check
    CHECK(cpu_step(cpu) != 0); // push b
    CHECK(cpu_get_stack_size(cpu) == 1);
    CHECK(stack_ptr[0] == 2);
    CHECK(cpu_step(cpu) != 0); // store c 0
    CHECK(cpu_get_stack_size(cpu) == 1);
    CHECK(stack_ptr[0] == 3);
    CHECK(cpu_step(cpu) != 0); // pop d
    CHECK(cpu_get_stack_size(cpu) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 3);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}
