#include "libs/cut.h"

#include <stdio.h>
#include <stdlib.h>

#include "libs/utils.h"
#include "../cpu.h"

/* Macros for file paths */
#define NOP_FILE  "../tests-cut/data/nanecisto-nop.bin" // hw03/build/../tests-cut/data/binary.bin
#define ADD_FILE  "../tests-cut/data/nanecisto-add.bin"
#define LOAD_FILE "../tests-cut/data/nanecisto-load.bin"

/* Functions from assignment */
/* #desc: cpu_create_memory */
TEST(nanecisto_cpu_create_memory)
{
    // Allocations
    FILE *fptr = fopen(ADD_FILE, "rb");
    ASSERT(fptr != NULL);
    size_t stack_capacity = 1;
    int32_t *stack_ptr = NULL;
    int32_t *memory = cpu_create_memory(fptr, stack_capacity, &stack_ptr);
    ASSERT(memory != NULL);

    // Check
    CHECK(memory[0] == 2); // add
    CHECK(memory[1] == 1); // reg b
    CHECK((stack_ptr - memory) * sizeof(int32_t) == 4096 - sizeof(int32_t)); // 4KiB is allocated

    // Close and destroy
    fclose(fptr);
    free(memory);
}

/* Functions from assignment */
/* #desc: cpu_create */
TEST(nanecisto_cpu_create)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, NOP_FILE) == 0);

    // Check
    CHECK(cpu_get_register(cpu, REGISTER_A) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);
    CHECK(cpu_get_status(cpu) == 0);
    CHECK(cpu_get_stack_size(cpu) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Functions from assignment */
/* #desc: cpu_destroy */
TEST(nanecisto_cpu_destroy)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, LOAD_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 4);

    CHECK(cpu_step(cpu) != 0); // push

    cpu_destroy(cpu);

    // Check
    CHECK(cpu_get_register(cpu, REGISTER_A) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);
    CHECK(cpu_get_status(cpu) == 0);
    CHECK(cpu_get_stack_size(cpu) == 0);

    fclose(fptr);
    free(cpu);
}

/* Functions from assignment */
/* #desc: cpu_reset */
TEST(nanecisto_cpu_reset)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, LOAD_FILE) == 0);

    // Setting registers
    cpu_set_register(cpu, REGISTER_A, 1);
    cpu_set_register(cpu, REGISTER_B, 2);
    cpu_set_register(cpu, REGISTER_C, 3);
    cpu_set_register(cpu, REGISTER_D, 4);

    CHECK(cpu_step(cpu) != 0); // push

    cpu_reset(cpu);

    // Check
    CHECK(cpu_get_register(cpu, REGISTER_A) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_B) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_C) == 0);
    CHECK(cpu_get_register(cpu, REGISTER_D) == 0);
    CHECK(cpu_get_status(cpu) == 0);
    CHECK(cpu_get_stack_size(cpu) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Functions from assignment */
/* #desc: cpu_status */
TEST(nanecisto_cpu_status)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, NOP_FILE) == 0);

    CHECK(cpu_get_status(cpu) == 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}

/* Functions from assignment */
/* #desc: cpu_step */
TEST(nanecisto_cpu_step)
{
    // Allocations
    FILE *fptr = NULL;
    struct cpu *cpu = NULL;
    ASSERT(prepare_cpu(&cpu, &fptr, NOP_FILE) == 0);

    CHECK(cpu_step(cpu) != 0);

    // Close and destroy
    fclose(fptr);
    cpu_destroy(cpu);
    free(cpu);
}
