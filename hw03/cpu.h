#ifndef CPU_H
#define CPU_H

#include <stdint.h>
#include <stdio.h>

enum cpu_status
{
    CPU_OK,
    CPU_HALTED,
    CPU_ILLEGAL_INSTRUCTION,
    CPU_ILLEGAL_OPERAND,
    CPU_INVALID_ADDRESS,
    CPU_INVALID_STACK_OPERATION,
    CPU_DIV_BY_ZERO,
    CPU_IO_ERROR
};

enum cpu_register {
    REGISTER_A,
    REGISTER_B,
    REGISTER_C,
    REGISTER_D,
#ifdef BONUS_JMP
    REGISTER_RESULT,
#endif // BONUS_JMP
};

struct cpu; // Hold your tongue!

// Something is missing...

#endif // CPU_H
