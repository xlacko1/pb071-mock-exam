#!/bin/false

# This file contains some basic CLI tests.
# These tests compare stdout of the program.
# Put your wanted program output in 'data/<test-name>.out'.
# Directory 'data' contains some example outputs of implementation on Aisa,
# yours output can be different.
# '-esucc' means that program should end successfully.
# '-0' defines stdin of program, in this case file with "Enters" to imitate user
# Find more at https://gitlab.fi.muni.cz/pb071/cli.

test_run_ok() {
	cpu run "$cli_data/nanecisto-add-halt.bin"
}

run -esucc 'Run on movr add halt' test_run_ok

test_run_big_capacity_ok() {
	cpu run 5000 "$cli_data/nanecisto-add-halt.bin"
}

run -esucc 'Run on movr add halt with 5000 stack capacity' test_run_big_capacity_ok

test_trace_ok() {
	cpu trace "$cli_data/nanecisto-add-halt.bin"
}

run -esucc -0 hundred-enters.txt 'Trace on movr add halt' test_trace_ok
