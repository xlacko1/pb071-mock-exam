; Simple instructions
;  → Arithmetic instructions

movr a 1
movr b 2
add b
halt

; vi:syntax=asm