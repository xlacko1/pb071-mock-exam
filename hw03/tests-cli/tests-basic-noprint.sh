#!/bin/false

# This file contains some basic CLI tests.
# These tests DO NOT compare stdout of the program.
# '-esucc' means that program should end successfully.
# '-0' defines stdin of program, in this case file with "Enters" to imitate user
# Find more at https://gitlab.fi.muni.cz/pb071/cli.

# Instead of comparing stdout, just check if program printed at least something.
suite_run_check_stdout() {
	test -s "$1"
}

test_run_ok_noprint() {
	cpu run "$cli_data/nanecisto-add-halt.bin"
}

run -esucc 'Run on movr add halt, noprint' test_run_ok_noprint

test_run_big_capacity_ok_noprint() {
	cpu run 5000 "$cli_data/nanecisto-add-halt.bin"
}

run -esucc 'Run on movr add halt with 5000 stack capacity, noprint' test_run_big_capacity_ok_noprint

test_trace_ok_noprint() {
	cpu trace "$cli_data/nanecisto-add-halt.bin"
}

run -esucc -0 hundred-enters.txt 'Trace on movr add halt, noprint' test_trace_ok_noprint
