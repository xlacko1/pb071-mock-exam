; digit sum with stack

in c

movr d 10

digit:
	push c
	pop a
	div d
	mul d
	swap a c
	sub c
	push a
	swap a c
	div d
	swap a c
	inc b
	loop digit

movr a 0
push b
pop c

sum:
	pop b
	add b
	dec c
	loop sum

out a
halt
