#!/bin/false

run 'Check small solvable' -- maze_check 'small_solvable.in'

run -efail -c stderr_not_empty 'Check no exit' -- maze_check 'no_exit.in'
