#ifndef LIBBMP_H
#define LIBBMP_H

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

/**
 * representation of RGB color
 */
struct libbmp_color
{
    uint8_t r; /**< red value */
    uint8_t g; /**< green value */
    uint8_t b; /**< blue value */
};

/**
 * Opaque definition of image object
 */
typedef struct _libbmp_image LIBBMP_IMAGE;

/**
 * Creates image object.
 *
 * An image object contains an information about the dimensions of the image,
 * which is specified by @p width and @p height,
 * and two-dimensional array of pixels. Each pixel stores a color value.
 *
 * The function will initialize every pixel to black color #000000.
 *
 * This object must be passed to libbmp_image_destroy function to properly
 * release all its allocated resources.
 *
 * @see libbmp_image_destroy
 *
 * @param width   width of the image
 * @param height  height of the image
 *
 * @return handler to image object on success;
 *         NULL otherwise
 */
LIBBMP_IMAGE *libbmp_image_create(uint32_t width, uint32_t height);

/**
 * Set pixel color at specified position.
 *
 * Position (x = 0, y = 0) corresponds to bottom-left corner of image.
 * Position (x = width - 1, y = height - 1) corresponds to
 * top-right corner.
 *
 * Function assumes that @p x and @p y are in range [0, width - 1]
 * and [0, height - 1] respectively.
 *
 *
 * @param image  handler to image object
 * @param x      x coordinate
 * @param y      y coordinate
 * @param color  pixel color to be set
 */
void libbmp_image_set_pixel(LIBBMP_IMAGE *image, size_t x, size_t y, struct libbmp_color color);

/**
 * Writes contents of an image to a file.
 *
 * @param image  handler to image object
 * @param file   handler to file opened for writing
 *
 * @return true if all data were written
 *         false otherwise
 */
bool libbmp_image_save(LIBBMP_IMAGE *image, FILE *file);

/**
 * Releases all resources held by @p image
 *
 * @param image  handler to image object
 */
void libbmp_image_destroy(LIBBMP_IMAGE *image);

#endif // LIBBMP_H
