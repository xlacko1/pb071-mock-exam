#ifndef MAZE_H
#define MAZE_H

#include <stdbool.h>
#include <stdio.h>

/* Feel free to implement and/or change the provided functions and structs. */

struct tile
{
    // TODO specify the tile's purpose
};

struct position
{
    // TODO define the role of position
};

struct maze
{
    // TODO add maze interpretation
};

/** fills maze based on the input @p file, returns true on success, false on
 * failure **/
bool maze_create(struct maze *maze, FILE *file);

/** cleans internal fields **/
void maze_destroy(struct maze *maze);

/** returns width of the maze **/
size_t maze_get_width(struct maze *maze);

/** returns height of the maze **/
size_t maze_get_height(struct maze *maze);

/** checks whether @p pos are valid coordinates for a tile **/
bool maze_is_within_bounds(struct maze *maze, struct position pos);

/** returns true if there is a tile on @p pos, also fills @p tile with a
 * copy of the tile, returns false otherwise **/
bool maze_get_tile(struct maze *maze, struct position pos, struct tile *tile);

/** copies the @p tile onto another tile located on @p pos
 * returns true on success, false on failure **/
bool maze_set_tile(struct maze *maze, struct position pos, struct tile tile);

/** fills @p adjecent_positions with positions adjecent to @p from **/
void maze_get_adjacent_positions(struct maze *maze, struct position from, struct position adjacent_positions[4]);

#endif // MAZE_H
