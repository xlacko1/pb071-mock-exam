#include "libbmp.h"
#include <assert.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

static const uint16_t _LIBBMP_BITS_PER_PIXEL = 24u;
static const uint32_t _LIBBMP_X_PIXELS_PER_METRE = 2835u;
static const uint32_t _LIBBMP_Y_PIXELS_PER_METRE = _LIBBMP_X_PIXELS_PER_METRE;

struct _libbmp_pixel_array
{
    uint8_t *data;
    size_t size;
    size_t width;
    size_t height;
    size_t row_size;
    size_t bytes_per_pixel;
};

struct _libbmp_header
{
    uint8_t content[14];
};

struct _libbmp_info_header
{
    uint8_t content[40];
};

struct _libbmp_image
{
    struct _libbmp_header header;
    struct _libbmp_info_header info_header;
    struct _libbmp_pixel_array pixels;
};

// Format uses little-endian.
static void _libbmp_write_half_word(uint8_t *data, uint16_t half_word)
{
    for (int i = 0; i < 2; ++i) {
        data[i] = (half_word >> (8u * i)) & 0xff;
    }
}

static void _libbmp_write_word(uint8_t *data, uint32_t word)
{
    for (int i = 0; i < 4; ++i) {
        data[i] = (word >> (8u * i)) & 0xff;
    }
}

static void _libbmp_header_init(struct _libbmp_header *header, uint32_t file_size, uint32_t data_offset)
{
    // Signature
    header->content[0] = 'B';
    header->content[1] = 'M';
    // Filesize
    _libbmp_write_word(&header->content[2], file_size);
    // Reserved
    memset(&header->content[6], 0, 4);
    // DataOffset
    _libbmp_write_word(&header->content[10], data_offset);
}

static void _libbmp_info_header_init(struct _libbmp_info_header *info_header, uint32_t width, uint32_t height)
{
    // Size
    _libbmp_write_word(&info_header->content[0], sizeof(info_header->content));
    // Width
    _libbmp_write_word(&info_header->content[4], width);
    // Height
    _libbmp_write_word(&info_header->content[8], height);
    // Planes
    _libbmp_write_half_word(&info_header->content[12], 1u);
    // Bits Per Pixel
    _libbmp_write_half_word(&info_header->content[14], _LIBBMP_BITS_PER_PIXEL);
    // Compression
    _libbmp_write_word(&info_header->content[16], 0u);
    // ImageSize
    _libbmp_write_word(&info_header->content[20], 0u);
    // XpixelsPerM
    _libbmp_write_word(&info_header->content[24], _LIBBMP_X_PIXELS_PER_METRE);
    // YpiselsPerM
    _libbmp_write_word(&info_header->content[28], _LIBBMP_Y_PIXELS_PER_METRE);
    // Colors Used
    _libbmp_write_word(&info_header->content[32], 0u);
    // Important Colors
    _libbmp_write_word(&info_header->content[36], 0u);
}

static bool _libbmp_pixel_array_create(struct _libbmp_pixel_array *pixel_array, size_t width, size_t height)
{
    size_t row_size = ((_LIBBMP_BITS_PER_PIXEL * width + 31) / 32) * 4;
    size_t size = row_size * height;
    uint8_t *data = calloc(size, sizeof(*data));
    if (data == NULL) {
        return false;
    }

    pixel_array->size = size;
    pixel_array->bytes_per_pixel = _LIBBMP_BITS_PER_PIXEL / 8;
    pixel_array->row_size = row_size;
    pixel_array->height = height;
    pixel_array->width = width;
    pixel_array->data = data;
    return true;
}

LIBBMP_IMAGE *libbmp_image_create(uint32_t width, uint32_t height)
{
    assert(width > 0);
    assert(height > 0);
    LIBBMP_IMAGE *image = malloc(sizeof(*image));
    if (image == NULL) {
        return NULL;
    }

    if (!_libbmp_pixel_array_create(&image->pixels, width, height)) {
        free(image);
        return NULL;
    }

    uint32_t headers_size = sizeof(image->header.content) + sizeof(image->info_header.content);
    uint32_t file_size = headers_size + image->pixels.size;
    _libbmp_header_init(&image->header, file_size, headers_size);
    _libbmp_info_header_init(&image->info_header, width, height);

    return image;
}

void libbmp_image_set_pixel(LIBBMP_IMAGE *image, size_t x, size_t y, struct libbmp_color color)
{
    assert(image != NULL);
    assert(x < image->pixels.width);
    assert(y < image->pixels.height);

    uint8_t *pixel = &image->pixels.data[y * image->pixels.row_size + x * image->pixels.bytes_per_pixel];
    pixel[0] = color.b;
    pixel[1] = color.g;
    pixel[2] = color.r;
}

bool libbmp_image_save(LIBBMP_IMAGE *image, FILE *file)
{
    assert(image != NULL);
    assert(file != NULL);

    if (fwrite(image->header.content, sizeof(uint8_t), sizeof(image->header.content), file) != sizeof(image->header.content)) {
        return false;
    }

    if (fwrite(image->info_header.content, sizeof(uint8_t), sizeof(image->info_header.content), file) != sizeof(image->info_header.content)) {
        return false;
    }
    if (fwrite(image->pixels.data, sizeof(uint8_t), image->pixels.size, file) != image->pixels.size) {
        return false;
    }

    return true;
}

void libbmp_image_destroy(LIBBMP_IMAGE *image)
{
    free(image->pixels.data);
    free(image);
}
