#!/bin/sh

script_dir=$(dirname "$0")
git_root=$(git rev-parse --prefix "$script_dir" --show-toplevel)
hooks_dir="$script_dir/hooks"

errors=0
actions_required=0

ok() {
    printf "\033[32m$@\033[0m\n"
}

warning() {
    printf "\033[33m$@\033[0m\n" >&2
}

error() {
    printf "\033[31m$@\033[0m\n" >&2
    : $((errors += 1))
}

fatal() {
    printf "\033[31m$@\033[0m\n" >&2
    exit 2
}

install() {
    if [ ! -f "$2" ] || ! diff "$1" "$2" >/dev/null; then
        if [ -f "$2" ]; then
            echo "Updating $1"
        else
            echo "Installing $1"
        fi

        echo "  -> $2"

        if ! cp -i "$1" "$2"; then
            error "Failed to $verb $1!"
        fi
    fi
}

setup_hooks() {
    for file in "$hooks_dir"/*; do
        install "$file" "$git_root/.git/hooks/$(basename "$file")"
    done
}

_setup_config_pull_rebase() {
    pull_rebase=$(git config --get pull.rebase)

    if [ $? -ne 0 ] || [ "$pull_rebase" != 'false' ]; then
        if git -C "$git_dir" config pull.rebase false; then
            echo "Disabled 'rebase' method for 'git pull'"
        else
            error "Cannot disable 'rebase' method for 'git pull'"
        fi
        echo "    (previous value was '$pull_rebase')"
    fi
}

_setup_config_pull_ff() {
    pull_ff=$(git config --get pull.ff)

    # Unset ‹pull.ff› is also OK.
    if [ $? -eq 0 ] && ( [ -z "$pull_ff" ] && [ "$pull_ff" != 'true' ] ); then
        if git -C "$git_dir" config pull.ff true; then
            echo "Enabled 'pull.ff' option"
        else
            error "Cannot enable 'pull.ff' option"
        fi

        echo "    (previous value was '$pull_ff')"
    fi
}

_setup_config_identity_message() {
    if [ "$_setup_config_has_name" -ge 1 ] && [ "$_setup_config_has_email" -ge 1 ]; then
        return
    fi

    echo "Before you start creating further commits, please set up your"
    echo "identity properly using the following command(s):"
    echo
    if [ "$_setup_config_has_name" -eq 0 ]; then
        echo "    git config user.name 'Your Name'"
    fi

    if [ "$_setup_config_has_email" -eq 0 ]; then
        echo "    git config user.email 'YourLogin@fi.muni.cz"
    fi

    echo
    echo "Substitute 'Your Name' and 'YourLogin' with your faculty identity."
}

_setup_config_has_name=0
_setup_config_has_email=0

_setup_config_user_name() {
    user_name=$(git config --get user.name)

    # Names can be really wild (see [1]), so let us just assume that it should
    # likely be different from '$USER'.
    # [1] https://www.kalzumeus.com/2010/06/17/falsehoods-programmers-believe-about-names/
    if [ $? -ne 0 ] || [ -z "$user_name" ] || [ "x$user_name" = "x$USER" ]; then
        warning "User name appears to be unset!"
        : $((actions_required += 1))
    else
        _setup_config_has_name=1
    fi
}

_setup_config_user_email() {
    user_email=$(git config --get user.email)

    # E-mail should resemble 'LOGIN@fi.muni.cz'.
    if [ $? -ne 0 ] || [ -z "$user_email" ]; then
        warning "User e-mail appears to be unset!"
        : $((actions_required += 1))
    elif ! echo "$user_email" | grep -qE "^[a-zA-Z0-9]+@fi\.muni\.cz$"; then
        warning "User e-mail does not appear to be a FI e-mail!"
        : $((actions_required += 1))
    else
        _setup_config_has_email=1
    fi
}

setup_config() {
    _setup_config_pull_rebase
    _setup_config_pull_ff

    _setup_config_user_name
    _setup_config_user_email
    _setup_config_identity_message
}

opt_hooks=1
opt_config=1

while getopts "HC" option; do
    case "$option" in
    H)
        opt_hooks=0
        ;;
    C)
        opt_config=0
        ;;
    *)
        fatal "usage: $0 [-HC]"
    esac
done

if [ $opt_hooks -eq 1 ]; then
    if [ ! -d "$hooks_dir" ]; then
        fatal "Hooks directory does not exist!"
    fi

    setup_hooks
fi

if [ $opt_config -eq 1 ]; then
    setup_config
fi

if [ $errors -gt 0 ]; then
    fatal "Setup failed!"
elif [ $actions_required -gt 0 ]; then
        echo
        warning "Setup completed successfully, but further actions are required!"
        echo "Please, carefully review the output of this script and perform the actions."
        echo "Then, re-run this script to verify the configuration."
else
        ok "Setup completed successfully."
fi
