#!/bin/sh

set -e

SCRIPT_DIR=$(dirname "$0")
GIT_ROOT=$(git rev-parse --prefix "$SCRIPT_DIR" --show-toplevel)
HOOKS_DIR="$SCRIPT_DIR/hooks"

echo "Executing $SCRIPT_DIR/setup.sh instead"
exec "$SCRIPT_DIR/setup.sh"
