#!/bin/false

### Basic functionality

test_encode1() {
    # `test_encode` runs a helper function which takes the input string as
    # an argument. See `cli-utils.sh`
    test_encode "Man"
}
run 'Encode "Man"' test_encode1

test_encode2() {
    test_encode "Amet"
}
run 'Encode "Amet"' test_encode2

test_encode3() {
    test_encode "Donec nec euismod orci"
}
run 'Encode "Donec..."' test_encode3

test_encode5() {
    test_encode "Lorem ipsum dolor sit amet, consectetur adipiscing elit"
}
run 'Encode "Lorem ipsum..."' test_encode5

test_decode1() {
    test_decode "T1C#6"
}
run 'Decode to "Amet"' test_decode1

test_decode2() {
    # We have to duplicate % so printf doesn't mistake it for a format sign
    test_decode 'Uu]i:26K(F*tbD+6rTfD-",!F)FV5B6Jo(F%%jN+E$UuSA6>c)F'
}
run 'Decode to "Praesent..."' test_decode2
### Try some edge cases

## Test for non-text input
test_encode_nonalpha() {
    test_encode "\001\002\003\004"
}
run "Encode non-printable characters" test_encode_nonalpha

## Test that newline doesn't change decode
test_decode_newline() {
    test_decode "T1C#6\n"
}
run "Decode with newline" test_decode_newline


## Test that decode fails on invalid encoding
test_decode_invalid() {
    test_decode "87c~R"
}
run -efail "Decode with invalid input" test_decode_invalid
