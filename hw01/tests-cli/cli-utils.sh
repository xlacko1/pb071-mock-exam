#!/bin/false


#   ┏━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
#   ┃   𝐃𝐎 𝐍𝐎𝐓 𝐄𝐃𝐈𝐓 𝐓𝐇𝐈𝐒 𝐅𝐈𝐋𝐄   ┃
#   ┗━━━━━━━━━━━━━━━━━━━━━━━━━━━┛


# This file contains helper functions for your tests. Write your tests in
# tests-*.sh instead.

if [ -n "${_cli_test_utils+loaded}" ]; then
	return
fi

_cli_test_utils=1

_exe="${cli_script_bin}"/ascii85

ascii85() {
	timeout --kill-after 5 ${CLI_TEST_TIMEOUT:-30} \
	valgrind --log-file="$cli_stash/valgrind.log" --errors-for-leak-kinds=all \
		 --leak-check=full --show-leak-kinds=all --track-fds=yes \
	"${_exe}" "$@"
}

exe="ascii85"

tests_init() {
	if [ ! -x "${_exe}" ]; then
		_cli_msg_fatal "Executable '$_exe' not found"
	fi
}

# Helper functions
test_encode() {
    # $# checks the number of parameters passed to the function, -ne stands
    # for non-equal
	if [ $# -ne 1 ]; then
        # This line sends the string to the standard error output
		echo "usage: test_encode 'INPUT'" >&2
		exit 1
	fi

    # Send the string passed as the first parameter to our program
    printf "$1" | ascii85 -e
}

test_decode() {

	if [ $# -ne 1 ]; then
		echo "usage: test_decode 'INPUT'" >&2
		exit 1
	fi
    printf "$1" | ascii85 -d
}
